/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo.ENTITIES;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javafx.scene.control.DatePicker;

/**
 *
 * @author Seddik
 */
public class Service {

    private int id;
    private String titre;
    private String lieu;
    private String description;
    private String etat ;
    private String date;
    private String dates;
    private DateFormat datef = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
     private Calendar cal= Calendar.getInstance();
    public Service() {

    }

    public Service(int id, String titre, String lieu,String etat,String description,String date,String dates) {
        this.id = id;
        this.titre = titre;
        this.lieu = lieu;
        this.etat = etat;
        this.description = description;
        this.date=date;
        this.dates=dates;

    }

    public String getDates() {
        return dates;
    }

    public void setDates(String dates) {
        this.dates = dates;
    }

     public String getDatef() {
       return datef.format(cal.getTime());
       
    }
     
     public String getDate(){
         return date;
     }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Service{" + "id=" + id + ", titre=" + titre + ", lieu=" + lieu +  ", etat=" + etat + ", description=" + description +  ", Date="+date+ ", Date de Service est=" +dates+'}';
    }
    
    
}
