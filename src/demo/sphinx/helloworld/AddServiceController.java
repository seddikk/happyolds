/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo.sphinx.helloworld;

import javafx.fxml.Initializable;
import java.util.ResourceBundle;
import java.net.URL;
import demo.CRUD.ServiceCrud;
import demo.ENTITIES.Service;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import com.sun.prism.paint.Color;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import static javafx.scene.paint.Color.color;
import javafx.util.converter.LocalDateStringConverter;
import org.controlsfx.control.textfield.TextFields;

/**
 * FXML Controller class
 *
 * @author Seddik
 */
public class AddServiceController implements Initializable {

    @FXML
    private TextField titreid;
    @FXML
    private TextField lieuid;
    @FXML
    private TextArea descid;
    @FXML
    private Button valid;
    @FXML
    private Label ctitre;
    @FXML
    private Label clieu;
    @FXML
    private Label cdescription;
    @FXML
    private DatePicker dateid;
    @FXML
    private Label cdate;
    @FXML
    private Button Rid;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        String[] possiblewords = {"Tunis","Ariana","Ben Arous","Mannouba","Nabeul","Bizerte","El Kef","Jandouba","Beja","Sfax","Mahdya","Sousse","Ben Gerden","El Kairaouen","Gafsa","Gabes","Selyena","Medenine","Monastir","Zaghouen","Tozeur","Sidi Bouziid","Tatawin","Kebilli","Menzel Salem","LaSoukra","SidiHassine","ElMourouj","Gafsa","Raoued","Aïn Djeloula",	
"Echrarda",	
"Beni M'Tir",	
 "Aachech",
 "Al Ahouaz-El Assouda",	
 "Belkhir",	
 "Bouchemma",	
 "El Amra",	
 "El Ayoun",		
 "Ennasr",		
 "Ezzouhour",	
 "Essaïda",	
 "Faouar",	
 "Fouchana",		
 "Ghezala",	
 "Hadjeb",	
 "Hassi El Ferid",	
 "Hazeg Ellouza",	
 "Hazoua",	
 "Hkaima",	
 "Joumine",	
 "Lela",	
 "Menzel El Habib",	
 "Mohamedia",		
 "Mnihla",
 "Nadhour Sidi Ali Ben Abedslem",	
 "Ouabed Khazanet",	
 "Rjim Maatoug",	
 "Saouaf",	
 "Sidi Aïch",	
 "Sidi Zid",	
 "Smâr",	
 "Souk Jedid",	
 "Teboulbou",	
 "Thibar",	
 "Tlelsa",
 "Utique",	
 "Zarzis Nord",	
 "Zelba",};
      TextFields.bindAutoCompletion(lieuid,possiblewords);
        
        

        // TODO
    }

    @FXML
    private void insertService(ActionEvent event) throws IOException {
         

        // else if(titreid.getText().isEmpty()){
        if ((titreid.getText().isEmpty()) && (lieuid.getText().isEmpty()) && (descid.getText().isEmpty())&& (dateid.getEditor().getText().isEmpty())) {
            ctitre.setText("veuillez vérifier le champ titre");
            clieu.setText("veuilez verifier le champ lieu");
            cdescription.setText("veuillez vérifier le champ description");
            cdate.setText("veuillez indiquer la date quand vous souhaitez le service");
        } else if ((titreid.getText().isEmpty()) && (lieuid.getText().isEmpty())) {
            ctitre.setText("veuillez vérifier le champ titre");
            clieu.setText("veuilez verifier le champ lieu");
        } else if ((lieuid.getText().isEmpty()) && (descid.getText().isEmpty())) {
            clieu.setText("veuilez verifier le champ lieu");
            cdescription.setText("veuillez vérifier le champ description");
        } else if ((descid.getText().isEmpty()) && (titreid.getText().isEmpty())) {
            ctitre.setText("veuillez vérifier le champ titre");
            cdescription.setText("veuillez vérifier le champ description");
        }  
            
            
            
            else if (dateid.getEditor().getText().isEmpty()) {
            cdate.setText("veuillez indiquer la date quand vous souhaitez le service");
            
            }
            
            
        else 
        {
            Service s = new Service();
            
            s.setTitre(titreid.getText());
            s.setLieu(lieuid.getText());
            s.setEtat("En Attente");
            s.setDescription(descid.getText());
            s.setDate(s.getDatef());
            s.setDates(dateid.getEditor().getText());
            
            

            ServiceCrud pcrud = new ServiceCrud();
            ServiceCrud.ajouterService(s);
            
            
           FXMLLoader loader = new FXMLLoader(getClass().getResource("listeServices.fxml"));
           Parent root = loader.load();
           ListeServicesController ls = loader.getController();
            
           valid.getScene().setRoot(root);
            
        }
    }
    
    @FXML
    private void Retourner(ActionEvent event) throws IOException{
        
        
        FXMLLoader loader = new FXMLLoader(getClass().getResource("ChoixCrudService.fxml"));
           Parent root = loader.load();
           ChoixCrudServiceController ccs = loader.getController();
            
           valid.getScene().setRoot(root);
        
        
        
        
    }
           
          
    
    

    // FXMLLoader loader = new FXMLLoader(getClass().getResource("readPerson.fxml"));
    // Parent root = loader.load();
    // ReadServiceController rc = loader.getController();
    //rc.setIdaff(titreid.getText());
    // titreid.getScene().setRoot(root);
    // } catch (IOException ex) {
    //  Logger.getLogger(AddServiceController.class.getName()).log(Level.SEVERE, null, ex);
    //  }
}
