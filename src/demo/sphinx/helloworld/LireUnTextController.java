/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo.sphinx.helloworld;

import HO.cnx.API.Read_File;
import HO.cnx.API.SyncPipe;
import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;
import com.github.sarxos.webcam.WebcamResolution;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javax.imageio.ImageIO;
import javax.swing.JFrame;

/*
import HO.cnx.API.Read_File;
import HO.cnx.API.SyncPipe;
import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;
import com.github.sarxos.webcam.WebcamResolution;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
*/
/**
 * FXML Controller class
 *
 * @author Seddik
 */
public class LireUnTextController implements Initializable {

    @FXML
    private TextArea chtext;

    public TextArea getChtext() {
        return chtext;
    }

    public void setChtext(String chtext) {
        this.chtext.setText(chtext);
    }
    @FXML
    private Button capt;
    @FXML
    private Button demar;
    @FXML
    private Button Retour;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void CapLi(ActionEvent event) throws IOException {
        Webcam webcam = Webcam.getDefault();
        //webcam.open();
        ImageIO.write(webcam.getImage(), "JPG",new File("capture.jpg"));
        
       System.out.println("photo capturée avec succés");
       webcam.close();
       
        String input_file="C:\\Users\\Seddik\\Documents\\NetBeansProjects\\HappyOld\\capture.jpg";
 String output_file="C:\\Users\\Seddik\\Documents\\NetBeansProjects\\HappyOld\\capture";
 String tesseract_install_path="D:\\Tesseract-OCR\\tesseract";
 String[] command =
    {
        "cmd",
    };
    Process p;
 try {
 p = Runtime.getRuntime().exec(command);
        new Thread(new SyncPipe(p.getErrorStream(), System.err)).start();
        new Thread(new SyncPipe(p.getInputStream(), System.out)).start();
        PrintWriter stdin = new PrintWriter(p.getOutputStream());
        stdin.println("\""+tesseract_install_path+"\" \""+input_file+"\" \""+output_file+"\" -l eng");
        stdin.close();
        p.waitFor();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println(Read_File.read_a_file(output_file+".txt"));
        
        setChtext(Read_File.read_a_file(output_file+".txt"));
    } catch (Exception e) {
 e.printStackTrace();
    }
       
        
    }
    
    
    
    
    @FXML
    private void Retouner(ActionEvent event) throws IOException {
         FXMLLoader loader = new FXMLLoader(getClass().getResource("ChoixCrudService.fxml"));
            Parent root = loader.load();
            ChoixCrudServiceController ccs = loader.getController();
            
            
        Retour.getScene().setRoot(root);
    }
    
    @FXML
    private void Demarrer(ActionEvent event)  {
        
         Webcam webcam = Webcam.getDefault();
        webcam.setViewSize(WebcamResolution.VGA.getSize());
        
        WebcamPanel panel = new WebcamPanel(webcam);
        panel.setFPSDisplayed(true);
        panel.setDisplayDebugInfo(true);
        panel.setImageSizeDisplayed(true);
        panel.setMirrored(false);
        
        JFrame window = new JFrame("Capture");
        window.add(panel);
        window.setResizable(true);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.pack();
        window.setLocationRelativeTo(null);
        window.setVisible(true);
    
    }

    
    
    
}
