/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo.sphinx.helloworld;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Seddik
 */
public class HomeUserController implements Initializable {

    @FXML
    private Label UserName;
    @FXML
    private Label logOut;
    @FXML
    private Label title;
    @FXML
    private Button btn01;
    @FXML
    private Button CService;
    @FXML
    private AnchorPane holderPane;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void logOutAction(MouseEvent event) {
    }

    @FXML
    private void profileAction(ActionEvent event) {
    }

    @FXML
    private void GoToService(ActionEvent event) throws IOException {
        
        
            FXMLLoader loader = new FXMLLoader(getClass().getResource("ChoixCrudService.fxml"));
            Parent root = loader.load();
            ChoixCrudServiceController ad = loader.getController();
            
            
        CService.getScene().setRoot(root);
            
        
        
        
        
    }
    
}
