/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo.sphinx.helloworld;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Seddik
 */
public class ChoixCrudServiceController implements Initializable {

    @FXML
    private Label UserName;
    @FXML
    private Label logOut;
    @FXML
    private AnchorPane holderPane;
    @FXML
    private Button AjouterS;
    @FXML
    private Button ListerS;
    @FXML
    private Button LireDoc;
    
    @FXML
    private Button br;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void logOutAction(MouseEvent event) {
    }

    @FXML
    private void InterfaceAjouter(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("addService.fxml"));
            Parent root = loader.load();
            AddServiceController ad = loader.getController();
            
            
        AjouterS.getScene().setRoot(root);
    }

    @FXML
    private void InterfaceListe(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("listeServices.fxml"));
            Parent root = loader.load();
            ListeServicesController ls = loader.getController();
            
            
        ListerS.getScene().setRoot(root);
    }

    

    @FXML
    private void Retourner(ActionEvent event) throws IOException {
         FXMLLoader loader = new FXMLLoader(getClass().getResource("HomeUser.fxml"));
            Parent root = loader.load();
            HomeUserController hu = loader.getController();
            
            
        br.getScene().setRoot(root);
    }
    
    @FXML
    private void LireDoc(ActionEvent event) throws IOException {
         FXMLLoader loader = new FXMLLoader(getClass().getResource("LireUnText.fxml"));
            Parent root = loader.load();
            LireUnTextController lt = loader.getController();
            
            
        br.getScene().setRoot(root);
    }
    
    
    
}
