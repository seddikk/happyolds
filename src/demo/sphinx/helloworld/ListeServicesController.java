/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo.sphinx.helloworld;

import demo.CRUD.ServiceCrud;
import demo.ENTITIES.Service;
import static com.sun.xml.internal.fastinfoset.alphabet.BuiltInRestrictedAlphabets.table;
import java.io.IOException;
import java.net.URL;
import java.util.Observable;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTreeCell;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.scene.input.MouseEvent;
import demo.sphinx.helloworld.AddServiceController;
import java.util.function.Predicate;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.KeyEvent;
/**
 * FXML Controller class
 *
 * @author Seddik
 */
public class ListeServicesController implements Initializable {

    @FXML
    private TableColumn<Service, String> col_titre;
    @FXML
    private TableColumn<Service, String> col_lieu;
    @FXML
    private TableColumn<Service, String> col_description;
    @FXML
    private TableColumn<Service, String> c_etat;
    @FXML
    private TableColumn<Service, String> c_date;
    @FXML
    private TableColumn<Service, String> col_numero;
    @FXML
    private TextField search;
    @FXML
    private TableColumn<Service, String> col_ds;
     Service s = new Service();
     ObservableList<Service> listS = FXCollections.observableArrayList();
    @FXML
    private TableView<Service> table;
    @FXML
    private Button supprimer;
    @FXML
    private Button modifier;
    @FXML
    private Button Executer;
    @FXML
    private Button Terminer;
    @FXML
    private Button Retour;
    
    int index;
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    
    
    
    FilteredList filter = new FilteredList(listS,e->true);
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
         ArrayList<Service> le = (ArrayList<Service>) ServiceCrud.afficherService();
        for(Service s:le)
        {
            listS.add(s);
        }
        col_numero.setCellValueFactory(new PropertyValueFactory<>("id"));
        col_titre.setCellValueFactory(new PropertyValueFactory<>("titre"));
        col_lieu.setCellValueFactory(new PropertyValueFactory<>("lieu"));
        col_description.setCellValueFactory(new PropertyValueFactory<>("description"));
        c_etat.setCellValueFactory(new PropertyValueFactory<>("etat"));
        c_date.setCellValueFactory(new PropertyValueFactory<>("date"));
        col_ds.setCellValueFactory(new PropertyValueFactory<>("dates"));
        
        table.setItems(listS);
        table.setEditable(true);
        
        
        
        col_titre.setCellFactory(TextFieldTableCell.forTableColumn());
        col_lieu.setCellFactory(TextFieldTableCell.forTableColumn());
        col_description.setCellFactory(TextFieldTableCell.forTableColumn());
        col_ds.setCellFactory(TextFieldTableCell.forTableColumn());
  
    }    


    
          /* Parent  root = null;

           FXMLLoader loader = new FXMLLoader(getClass()
                    .getResource("addSevice.fxml"));
        
        try {
            root = loader.load();
            AddServiceController ae = loader.getController();         
            btnretour.getScene().setRoot(root);
        } catch (IOException ex) {
            Logger.getLogger(AjouterEvenementController.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }
    
        // TODO
*/

    @FXML
    private void modifierService(ActionEvent event) {
       
        
        
       
        
       
        
    }

    @FXML
    private void supprimerService(ActionEvent event) {
        int id = table.getSelectionModel().getSelectedItem().getId();
        int index = table.getSelectionModel().getSelectedIndex();
        Service s = new Service();
        s.setId(id);
        ServiceCrud.supprimerService(id);
        table.getItems().remove(index);
        
    }


   
    
    

    

    @FXML
    private void OneditT(TableColumn.CellEditEvent<Service,String> event) {
        Service s = table.getSelectionModel().getSelectedItem();
        //System.out.println(s.toString());
        Service s1 = new Service();
        s1.setTitre(event.getNewValue());

        //System.out.println(event.getNewValue());
        //s.setId(table.getSelectionModel().getSelectedItem().getId());
        //System.out.println(s.getId());
        
        
        ServiceCrud scrud = new ServiceCrud();
        ServiceCrud.modifierTitre(s1,s.getId());
    }
    
    @FXML
    private void OneditL(TableColumn.CellEditEvent<Service,String> event) {
         Service s = table.getSelectionModel().getSelectedItem();
        //System.out.println(s.toString());
        Service s1 = new Service();
        s1.setLieu(event.getNewValue());

        //System.out.println(event.getNewValue());
        //s.setId(table.getSelectionModel().getSelectedItem().getId());
        //System.out.println(s.getId());
        
        
        ServiceCrud scrud = new ServiceCrud();
        ServiceCrud.modifierLieu(s1,s.getId());
    }

    @FXML
    private void OneditDe(TableColumn.CellEditEvent<Service,String> event) {
        Service s = table.getSelectionModel().getSelectedItem();
        //System.out.println(s.toString());
        Service s1 = new Service();
        s1.setDescription(event.getNewValue());

        //System.out.println(event.getNewValue());
        //s.setId(table.getSelectionModel().getSelectedItem().getId());
        //System.out.println(s.getId());
        
        
        ServiceCrud scrud = new ServiceCrud();
        ServiceCrud.modifierDescription(s1,s.getId());
    }

    @FXML
    private void OneditD(TableColumn.CellEditEvent<Service,String> event) {
        Service s = table.getSelectionModel().getSelectedItem();
        //System.out.println(s.toString());
        Service s1 = new Service();
        s1.setDate(event.getNewValue());

        //System.out.println(event.getNewValue());
        //s.setId(table.getSelectionModel().getSelectedItem().getId());
        //System.out.println(s.getId());
        
        
        ServiceCrud scrud = new ServiceCrud();
        ServiceCrud.modifierDate(s1,s.getId());
        
    }

    @FXML
    private void OneditE(TableColumn.CellEditEvent<Service,String> event) {
    }
       
    
    @FXML
    private void searchliste(KeyEvent event) {
       search.textProperty().addListener((observable,oldValue,newValue)->{
       filter.setPredicate((Predicate<? super Service>)(Service s)->{
       if (newValue.isEmpty() || newValue==null){
           return true;
            }
       else if (s.getLieu().startsWith(newValue) || s.getDates().startsWith(newValue) || 
               s.getDate().startsWith(newValue) || s.getEtat().startsWith(newValue))
       {
           return true;
       }
           return false;
       } );
       });
        SortedList sort=new SortedList(filter);
        sort.comparatorProperty().bind(table.comparatorProperty());
        table.setItems(sort);
    }
    
     @FXML
    private void Retouner(ActionEvent event) throws IOException{
        
        
        FXMLLoader loader = new FXMLLoader(getClass().getResource("ChoixCrudService.fxml"));
           Parent root = loader.load();
           ChoixCrudServiceController ccs = loader.getController();
            
           Retour.getScene().setRoot(root);
        
        
        
        
    }
    
    
     @FXML
    private void ExecuterService(ActionEvent event) throws IOException{
        
        int id = table.getSelectionModel().getSelectedItem().getId();
        //String etat=table.getSelectionModel().getSelectedItem().getEtat();
        int index = table.getSelectionModel().getSelectedIndex();
        Service s = new Service();
        s.setEtat("En Cours");
        ServiceCrud scrud=new ServiceCrud();
        scrud.modifierEtat(s,id);
        
        FXMLLoader loader = new FXMLLoader(getClass().getResource("listeServices.fxml"));
           Parent root = loader.load();
           ListeServicesController ccs = loader.getController();
        Executer.getScene().setRoot(root);
        
    }
    
    @FXML
    private void TerminerService(ActionEvent event) throws IOException{
        
        int id = table.getSelectionModel().getSelectedItem().getId();
        //String etat=table.getSelectionModel().getSelectedItem().getEtat();
        int index = table.getSelectionModel().getSelectedIndex();
        Service s = new Service();
        s.setEtat("Terminer");
        ServiceCrud scrud=new ServiceCrud();
        scrud.modifierEtat(s,id);
        table.setSelectionModel(null);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("listeServices.fxml"));
           Parent root = loader.load();
           ListeServicesController ccs = loader.getController();
        Executer.getScene().setRoot(root);
        
    }
    
    
}
