/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo.CRUD;

import demo.ENTITIES.Service;
import HO.cnx.tools.MyConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Seddik
 */
public class ServiceCrud {
    
    
    public static void ajouterService(Service s) {
        String requete = "INSERT INTO service_gratuit (titre , lieu , etat , description , Date , date_service ) VALUES ('" + s.getTitre()+"' ,'" +s.getLieu()+ "','" +s.getEtat()+ "','" +s.getDescription()+ "','" +s.getDate()+ "','" +s.getDates()+ "' )";
        MyConnection myCNX = MyConnection.getMyConnection();
        try {
            Statement st = myCNX.getCnx().createStatement();
            st.executeUpdate(requete);
            System.out.println("Service ajouté");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }

    }
    
    public static void supprimerService(int id){
        
        String requete2 = "DELETE FROM service_gratuit WHERE id=?";
        
        MyConnection myCNX =  MyConnection.getMyConnection();
        try {
            PreparedStatement pst =  myCNX.getCnx().prepareStatement(requete2);
           
            pst.setInt(1,id);
            pst.executeUpdate();
            System.out.println("service supprimer");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
        
        
        
    }
    
   public static void modifierService(Service s , int id){
        String requete3 = "UPDATE service_gratuit SET titre=?, lieu=?, etat=?, description=?, Date=?, date_service=?   WHERE id=?";
        MyConnection myCNX = MyConnection.getMyConnection();
        try {
            PreparedStatement pst = myCNX.getCnx().prepareStatement(requete3);
            
            
            pst.setString(1,s.getTitre());
           // pst.setString(2,s.getLieu());
           // pst.setString(3,s.getEtat());
          //  pst.setString(4,s.getDescription());
           // pst.setString(5,s.getDate());
          //  pst.setInt(5,id);
            
            pst.executeUpdate();
            
            System.out.println("Service modifier");
        } catch (SQLException ex) {
            
        }
    }
   
   
   
   public static void updateService(Service s)
    {
        
        int id = s.getId();
         String requete = "UPDATE service_gratuit SET titre=?, lieu=?, etat=?, description=?, Date=? WHERE id=?";
        
        MyConnection myCNX = MyConnection.getMyConnection();
        try
        {
            PreparedStatement pst = myCNX.getCnx().prepareStatement(requete);
            pst.setInt(1,s.getId());
            pst.setString(2,s.getTitre());
            pst.setString(3,s.getLieu());
            pst.setString(4,s.getEtat());
            pst.setString(5,s.getDescription());
            pst.setString(6,s.getDate());
            
            
            pst.executeUpdate();
            
            pst.close();

        } 
        catch (SQLException ex) 
        {
            System.out.println("service modifier");
            //Logger.getLogger(EvenementDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static List<Service> afficherService(){
        String requete4 = "SELECT * FROM service_gratuit ";
        MyConnection myCNX = MyConnection.getMyConnection();
        ArrayList<Service>myList = new ArrayList();
        try {
            Statement st = myCNX.getCnx().createStatement();
            ResultSet rs =st.executeQuery(requete4) ;
            while(rs.next()){
            Service s = new Service();
            s.setId(rs.getInt(1));
            s.setTitre(rs.getString(2));
            s.setLieu(rs.getString(3));
            s.setEtat(rs.getString(4));
            s.setDescription(rs.getString(5));
            s.setDate(rs.getString(6));
            s.setDates(rs.getString(7));
            myList.add(s);
            
            
            }
        }
        
        catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        
        
        return myList;
        
    }
    
    
    public static void modifierTitre(Service s , int id){
        String requeteT = "UPDATE service_gratuit SET titre=? WHERE id=?";
        MyConnection myCNX = MyConnection.getMyConnection();
        try {
            PreparedStatement pst = myCNX.getCnx().prepareStatement(requeteT);
            
            
            pst.setString(1,s.getTitre());
           // pst.setString(2,s.getLieu());
           // pst.setString(3,s.getEtat());
          //  pst.setString(4,s.getDescription());
           // pst.setString(5,s.getDate());
            pst.setInt(2,id);
            
          pst.executeUpdate();
            
            System.out.println("Titre modifier");
        } catch (SQLException ex) {
            
        }
    }
   
    
    public static void modifierLieu(Service s , int id){
        String requeteT = "UPDATE service_gratuit SET lieu=? WHERE id=?";
        MyConnection myCNX = MyConnection.getMyConnection();
        try {
            PreparedStatement pst = myCNX.getCnx().prepareStatement(requeteT);
            
              pst.setInt(2,id);
            //pst.setString(1,s.getTitre());
           pst.setString(1,s.getLieu());
           // pst.setString(3,s.getEtat());
          //  pst.setString(4,s.getDescription());
           // pst.setString(5,s.getDate());
          
            
           pst.executeUpdate();
            
            System.out.println("Lieu modifier");
        } catch (SQLException ex) {
            
        }
    }
    
     public static void modifierDescription(Service s , int id){
        String requeteDes = "UPDATE service_gratuit SET description=? WHERE id=?";
        MyConnection myCNX = MyConnection.getMyConnection();
        try {
            PreparedStatement pst = myCNX.getCnx().prepareStatement(requeteDes);
            
             pst.setInt(2,id);
           // pst.setString(2,s.getTitre());
          // pst.setString(3,s.getLieu());
        //   pst.setString(4,s.getEtat());
           pst.setString(1,s.getDescription());
          //  pst.setString(6,s.getDate());
          
            
           pst.executeUpdate();
            
            System.out.println("Description modifier");
        } catch (SQLException ex) {
            
        }
    }
     
     public static void modifierDate(Service s , int id){
        String requeteDes = "UPDATE service_gratuit SET date_service =? WHERE id=?";
        MyConnection myCNX = MyConnection.getMyConnection();
        try {
            PreparedStatement pst = myCNX.getCnx().prepareStatement(requeteDes);
            
             pst.setInt(2,id);
           // pst.setString(2,s.getTitre());
          // pst.setString(3,s.getLieu());
        //   pst.setString(4,s.getEtat());
           pst.setString(1,s.getDate());
          //  pst.setString(6,s.getDate());
          
            
           pst.executeUpdate();
            
            System.out.println("Date de creation modifier");
        } catch (SQLException ex) {
            
        }
    }
     
     public static void modifierEtat(Service s , int id){
        String requeteDes = "UPDATE service_gratuit SET etat =? WHERE id=?";
        MyConnection myCNX = MyConnection.getMyConnection();
        try {
            PreparedStatement pst = myCNX.getCnx().prepareStatement(requeteDes);
            
             pst.setInt(2,id);
           // pst.setString(2,s.getTitre());
          // pst.setString(3,s.getLieu());
         pst.setString(1,s.getEtat());
           //pst.setString(1,s.getDate());
          //  pst.setString(6,s.getDate());
          
            
           pst.executeUpdate();
            
            System.out.println("etat modifier");
        } catch (SQLException ex) {
            
        }
    }
     
     
}
