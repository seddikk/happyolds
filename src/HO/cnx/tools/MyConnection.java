/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HO.cnx.tools;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 *
 * @author Seddik
 */
public class MyConnection {
    
    String url="jdbc:mysql://localhost:3306/happy_olds";
    String login="root";
    String pwd="";
    Connection cnx;
    static MyConnection instance ;
    
    
    
    
    
    
    private MyConnection(){
        try {
            cnx= DriverManager.getConnection(url, login , pwd);
            System.out.println("Connexion établie !");
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }
    
    
    
    
    /*public void connectToDB(){
        try {
            cnx= DriverManager.getConnection(url, login , pwd);
            System.out.println("Connexion établie !");
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        
        
        
    }
    */
    public Connection getCnx(){
        
        return cnx;
        
    }
    
    public static MyConnection getMyConnection(){
        if (instance == null ){
            instance = new MyConnection();
        }
        
        return instance;
        
    }
    
    
}
